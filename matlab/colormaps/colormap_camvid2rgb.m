function [cm, labelstr, unused_idx, used_idx] = colormap_camvid2rgb

labelstr = {'void', 'animal', 'archway','bicyclist','bridge', 'building','car','cartluggagepram','child','column_pole','fence','lanemkgsdriv','lanemkgsnondriv','misc_text','motorcyclescooter','othermoving','parkingblock','pedestrian','road','roadshoulder','sidewalk','signsymbol','sky','suvpickuptruck','trafficcone','trafficlight','train','tree','truck_bus','tunnel','vegetationmisc','wall'}';

used_idx = [4 6 7 10 11 18 19 21 22 23 28]; % BICYCLIST, ROAD, BUILDING, SKY, TREE, SIDEWALK, CAR, COLUMN POLE, SIGN-SYMBOL, FENCE, PEDESTRIAN  
used_bit = false(length(labelstr),1);
used_bit(used_idx)=1;
unused_idx = find(~used_bit)';

        
% CAMVID color mapping for 32 classes (11 only used)
% 0 0 0		Void
% 64 128 64	Animal
% 192 0 128	Archway
% 0 128 192	Bicyclist
% 0 128 64	Bridge
% 128 0 0		Building
% 64 0 128	Car
% 64 0 192	CartLuggagePram
% 192 128 64	Child
% 192 192 128	Column_Pole
% 64 64 128	Fence
% 128 0 192	LaneMkgsDriv
% 192 0 64	LaneMkgsNonDriv
% 128 128 64	Misc_Text
% 192 0 192	MotorcycleScooter
% 128 64 64	OtherMoving
% 64 192 128	ParkingBlock
% 64 64 0		Pedestrian
% 128 64 128	Road
% 128 128 192	RoadShoulder
% 0 0 192		Sidewalk
% 192 128 128	SignSymbol
% 128 128 128	Sky
% 64 128 192	SUVPickupTruck
% 0 0 64		TrafficCone
% 0 64 64		TrafficLight
% 192 64 128	Train
% 128 128 0	Tree
% 192 128 192	Truck_Bus
% 64 0 64		Tunnel
% 192 192 0	VegetationMisc
% 64 192 0	Wall

cm = [
 0 0 0	; %Void
 64 128 64	; %Animal
 192 0 128	; %Archway
 0 128 192	; %Bicyclist
 0 128 64	; %Bridge
 128 0 0	; %	Building
 64 0 128	; %Car
 64 0 192	; %CartLuggagePram
 192 128 64	; %Child
 192 192 128; %	Column_Pole
 64 64 128	; %Fence
 128 0 192	; %LaneMkgsDriv
 192 0 64	; %LaneMkgsNonDriv
 128 128 64	; %Misc_Text
 192 0 192	; %MotorcycleScooter
 128 64 64	; %OtherMoving
 64 192 128	; %ParkingBlock
 64 64 0	; %	Pedestrian
 128 64 128	; %Road
 128 128 192; %	RoadShoulder
 0 0 192	; %	Sidewalk
 192 128 128; %	SignSymbol
 128 128 128; %	Sky
 64 128 192; %	SUVPickupTruck
 0 0 64		; %TrafficCone
 0 64 64	; %	TrafficLight
 192 64 128	; %Train
 128 128 0	; %Tree
 192 128 192	; %Truck_Bus
 64 0 64		; %Tunnel
 192 192 0	; %VegetationMisc
 64 192 0;  % wall
 ] /255; 


