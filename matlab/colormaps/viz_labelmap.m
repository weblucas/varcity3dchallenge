function im_labelmap = viz_labelmap(cm,labelstr)

cm = cm(1:length(labelstr),:);


w = 400; % image width
h = 70; % image height
m = 30; % max characters

reps=round((m-(cellfun(@length,labelstr)))/2);

% text
for idx=1: length(labelstr)
    labelstrtmp{idx}=[repmat(' ',1,reps(idx)) labelstr{idx} repmat(' ',1,reps(idx))];
end
labelstr=labelstrtmp';

position = [repmat(w/2,length(labelstr),1) (h/2:h:h*length(labelstr))'];

im_labelmap = insertText(ones(h*length(labelstr),w), position, labelstr, ...
    'FontSize', 40, 'BoxColor', cm, 'BoxOpacity', 1,'AnchorPoint','Center','TextColor',[0.2 0.2 0.2]);

% numbers
for idx=1: length(labelstr)
    labelstrtmp{idx}=num2str(idx-1);
end
labelstr=labelstrtmp';

position = [repmat(5,length(labelstr),1) (h/2:h:h*length(labelstr))'];

im_labelmap = insertText(im_labelmap, position, labelstr, ...
    'FontSize', 40, 'BoxColor', cm, 'BoxOpacity', 1,'AnchorPoint','LeftCenter','TextColor',[0.2 0.2 0.2]);
